json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :physician_id, :time_slot_id, :diagnostic_id, :total, :date, :complaint, :note
  json.url appointment_url(appointment, format: :json)
end
